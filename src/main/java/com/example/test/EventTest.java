package com.example.test;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class EventTest {

	public static void main(String[] args) {
		String expression = "(E1 AND (E2 OR E5))OR(E3ANDE4) OR (E5 AND E6)";
		List<String> smallExp = findExpression(expression);

		smallExp.forEach(System.out::println);
	}

	public static List<String> findExpression(String expression) {
		expression = expression.replaceAll("\\s", "").replaceAll("AND", "&").replaceAll("OR", "|");
		System.out.println("expression =" + expression);
		StringTokenizer tokenizer = new StringTokenizer(expression, "&|\\|\\(|\\)", true);
		int bracketCount = 0;
		String exp = "";
		List<String> smallExp = new ArrayList<>();
		while (tokenizer.hasMoreElements()) {
			String token = tokenizer.nextToken();
			// System.out.println(token);
			if (token.equals("(")) {
				bracketCount++;
				exp = exp + token;
			} else if (token.equals(")")) {
				bracketCount--;
				exp = exp + token;
				if (bracketCount == 0 && tokenizer.hasMoreTokens()) {
					int index = exp.indexOf("(");
					smallExp.add(exp.substring(index));
					// smallExp.add(exp);
					exp = "";
				} else if (bracketCount == 0 && !tokenizer.hasMoreTokens() && exp.charAt(0) != expression.charAt(0)) {
					int index = exp.indexOf("(");
					smallExp.add(exp.substring(index));
					// smallExp.add(exp);
					exp = "";
				} else if (bracketCount == 0 && !tokenizer.hasMoreTokens()) {
					smallExp = findExpression(expression.substring(1, expression.length() - 1));
					// recersive call
				}
			} else {
				exp = exp + token;
			}
		}

		return smallExp;

	}

}
